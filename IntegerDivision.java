import java.util.Scanner;
/**
* (c) Copyright Wasif Iqbal - all rights reserved.
* Write a function to perform integer division 
* without using either the / or * operators.
**/
public class IntegerDivision {
  public static boolean isNumeric(String s) {
    return s.matches("[-+]?\\d*\\.?\\d+");
  }
  public static int intDiv (int dividend, int divisor) {
    int quotient = 0;
    while (dividend > 0) {
      dividend -= divisor;
      quotient++;
    }
    return quotient;
  }

  public static int fastIntDiv(int dividend, int divisor) {
    int answer;
    int shiftCounter = 1;
    if (dividend < divisor || divisor == 0) {
      answer = 0;
    } else if (dividend == divisor) {
      answer = 1;
    } else if (divisor == 1) {
      answer = dividend;
    } else {
      answer = 0;
      while (divisor <= dividend) {
        divisor <<= 1;
        shiftCounter <<= 1;
      }

      divisor >>= 1;
      shiftCounter >>= 1;
    
      while (shiftCounter != 0) {
        if (dividend >= divisor) {
          dividend -= divisor;
          answer |= shiftCounter;
        }
        shiftCounter >>= 1;
        divisor >>= 1;
      }
    }
    return answer;
  }

  public static void main(String[] args) {
    int dividend, divisor;
    Scanner input = new Scanner(System.in);
    if (args.length > 1) {
      dividend = Integer.parseInt(args[0]);
      divisor = Integer.parseInt(args[1]);
      System.out.println(fastIntDiv(dividend, divisor));
    } 

    boolean complete = false;
    String usrStr;
    while (!complete) {    
      System.out.println("Enter dividend and divisor " +
                          "or type exit to leave the program: ");
      usrStr = input.next();
      while(usrStr.length() < 1) {
        usrStr = input.next();
      }

      if(isNumeric(usrStr)) {

        dividend = Integer.parseInt(usrStr);
        divisor = input.nextInt();
  
        System.out.println(fastIntDiv(dividend, divisor));
      } else {
        switch(usrStr) {
          case "exit":
          case "Exit":
          case "EXIT":
            System.exit(0);
            break;
          default:
            System.out.println("Invalid input entered. Please try again.");
        }
      }
    }
    System.exit(0);
  }
}
